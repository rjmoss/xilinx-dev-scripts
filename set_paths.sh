#!/bin/bash
# Script to setup paths to access Xilinx Development Scripts in this repository
echo "##### Executing ${BASH_SOURCE[0]} #####"

# Set this folder as the include path
XILINX_DEVSCRIPT_REPOBASE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export XILINX_DEVSCRIPT_REPOBASE

echo " - Adding Development Scripts to System PATH..."
###################################
# UPDATE PATH environment variable
###################################
# - Add Vitis Tool Scripts to PATH Variable
export PATH=$PATH:$XILINX_DEVSCRIPT_REPOBASE/vitis/scripts
export PATH=$PATH:$XILINX_DEVSCRIPT_REPOBASE/petalinux/scripts
export PATH=$PATH:$XILINX_DEVSCRIPT_REPOBASE/vivado/scripts
export PATH=$PATH:$XILINX_DEVSCRIPT_REPOBASE/vart/scripts
export PATH=$PATH:$XILINX_DEVSCRIPT_REPOBASE/target/scripts
export PATH=$PATH:$XILINX_DEVSCRIPT_REPOBASE/vvas/scripts
export PATH=$PATH:$XILINX_DEVSCRIPT_REPOBASE/media/scripts
export PATH=$PATH:$XILINX_DEVSCRIPT_REPOBASE/vitis_libraries/scripts
export PATH=$PATH:$XILINX_DEVSCRIPT_REPOBASE/tools/scripts
export PATH=$PATH:$XILINX_DEVSCRIPT_REPOBASE/vitis_ai_docker/scripts
########################################################################################
echo " - Updated PATH Environment Variable..."
