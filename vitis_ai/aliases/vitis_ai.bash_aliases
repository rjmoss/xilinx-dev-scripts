#!/bin/bash
# Declare Vitis AI Alias (functions)

# Check for presence of vitis ai runtime tools in the path
if [[ ! -f $(which vitis) ]]; then
	echo "WARNING: Vitis not located in system PATH (No Vitis Tools are available)."
fi

# Define command line function to checkout copy of Vitis AI Yocto Layer
# Note: This should be run in the root petalinux project folder
# xilinx_prep_vitis_ai_meta_layer () {
# 	# Repo: https://github.com/Xilinx/meta-vitis-ai
# 	# First check current folder for Petalinux Project
# 	TMP_DIR=$(pwd)	
# 	TMP_PLNX_VERSION=($(cat "$TMP_DIR/.petalinux/metadata" | cut -d '=' -f2))
# 	if [[ -z $TMP_PLNX_VERSION ]]; then
# 		echo "Petalinux Project Not Found (.petalinux/metadata is missing or corrupt)"
# 		echo "Please execute this script in the root folder of a valid Petalinux Project"
# 		exit 1
# 	else
# 		echo "Petalinux Project v$TMP_PLNX_VERSION detected"
# 	fi

# 	TMP_LAYER=$XILINX_DEVSCRIPT_VITIS_AI_META_LAYER
# 	TMP_REPO=$XILINX_DEVSCRIPT_VITIS_AI_META_LAYER_REPO
# 	TMP_TAG=$XILINX_DEVSCRIPT_VITIS_AI_META_LAYER_REPO_TAGPREFIX$TMP_PLNX_VERSION

# 	# Checkout the vitis ai layer from github
# 	echo "Checkout The Vitis AI Layer: $TMP_REPO"
# 	git clone  ./project-spec/$TMP_LAYER
# 	pushd ./project-spec/$TMP_LAYER
# 	git checkout -b working_$TMP_PLNX_VERSION 
# 	popd

# 	# Add layer to petalinux project

# 	# Add packagegroup to user layer

# 	# Enable petalinux package group in user layer
# 	# use existing helper script
# 	update_plnx_packagegroup petalinux-vitisai
# }

xilinx_help_vitis_ai_alias () {
	# Print
	printf "%-${XILINX_HELP_WIDTH_1}s | %-${XILINX_HELP_WIDTH_1}s\n" \
		"* Xilinx Vitis AI Aliases" "Description"
	printf "%.${XILINX_HELP_WIDTH_1}s | %.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR2 $XILINX_SEPARATOR2

	# Aliases List
	#printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
	#	" - xilinx_prep_vitis_ai_meta_layer" "Checkout the meta-vitis-ai layer and add to current petalinux project."
}

# List Aliases
xilinx_help_vitis_ai_alias

printf "\n"
