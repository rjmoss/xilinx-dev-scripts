#!/bin/bash
# Declare Vitis AI Runtime Alias (functions)

# Check for presence of vitis ai runtime tools in the path
if [[ ! -f $(which vitis) ]]; then
	echo "WARNING: Vitis not located in system PATH (No Vitis AI Runtime aliases will be available)."
fi

# Define command line function to prep the environment for Vitis AI Runtime SDK (Petalinux)
xilinx_prep_vart_sdk () {
	# Get the Vitis Version from the environment
	# Unset LD_LIBRARY_PATH pre-emptively per VART host cross compiler setup script
	# https://github.com/Xilinx/Vitis-AI/blob/master/setup/mpsoc/VART/host_cross_compiler_setup.sh
	unset LD_LIBRARY_PATH
	TMP_VER=$(basename $XILINX_VITIS)	
	source "/xilinx/local/shared/vitis-ai-sdk/petalinux_sdk_"$TMP_VER".0.0/environment-setup-cortexa72-cortexa53-xilinx-linux"
}

# Define command line function to prep a specific version of the environment for Vitis AI SDK (Petalinux)
xilinx_prep_vart_sdk_2021.1.0.0 () {
	# Unset LD_LIBRARY_PATH pre-emptively per VART host cross compiler setup script
	# https://github.com/Xilinx/Vitis-AI/blob/master/setup/mpsoc/VART/host_cross_compiler_setup.sh
	unset LD_LIBRARY_PATH
	source /xilinx/local/shared/vitis-ai-sdk/petalinux_sdk_2021.1.0.0/environment-setup-cortexa72-cortexa53-xilinx-linux
}

xilinx_help_vart_alias () {
	# Print
	printf "%-${XILINX_HELP_WIDTH_1}s | %-${XILINX_HELP_WIDTH_1}s\n" \
		"* Xilinx Vitis AI Runtime Aliases" "Description"
	printf "%.${XILINX_HELP_WIDTH_1}s | %.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR2 $XILINX_SEPARATOR2

	# Aliases List
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		" - xilinx_prep_vart_sdk" "Prep detected version of Vitis AI Runtime SDK (Petalinux - Installed)"
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		" - xilinx_prep_vart_sdk_2021.1.0.0" "Prep v2021.1.0 Vitis AI Runtime SDK (Petalinux - Installed)"
}

# List Aliases
xilinx_help_vart_alias

printf "\n"

#echo "Vitis AI Runtime Aliases:"
#echo "------------------------"
#declare -f | grep xilinx_prep_vart*
#echo "------------------------"
