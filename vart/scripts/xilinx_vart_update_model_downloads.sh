#!/bin/bash

# Reference: Model Download from the AI-Model-Zoo
# - https://github.com/Xilinx/Vitis-AI/tree/master/models/AI-Model-Zoo

# This Script parses the model.yaml file in each model directory under ./model-list/
# and downloads all associated model files for each supported target
# The script also checks the MD5SUM for each download
# as of 10/13/2021 there were 111 models, resulting in ~23.4GB of downloaded archives

_self="${0##*/}"

function show_opts {
	echo "Syntax:"
	echo "-------"
	echo "$_self --<option>"
	echo ""
	echo "Valid Options:"
	echo "--------------"
	echo "  --repo=<repository_location>"
	echo ""
	echo "      Use the specified repository location instead of the environment variable"
	echo ""
	echo "  --debug"
	echo ""
	echo "		Enable debug output"
	echo ""
	echo " --help"
	echo ""
	echo "      display this syntax help"
	echo ""
}

# Init command ling argument flags
FLAG_DEBUG=0 # Enable extra debug messages

# Process Command line arguments
OPT_REPO_OVERRIDE=""
PARAMS=""

while (("$#")); do
	case "$1" in
		--debug) # Enable debug output
			FLAG_DEBUG=1
			echo "Set: FLAG_DEBUG=$FLAG_DEBUG"
			shift
			;;
		--repo=*) # Override the repos location instead of using the environment variable setting
			OPT_REPO_OVERRIDE=$(echo $1 | cut -f2 -d "=")
			echo "INFO: REPO_OVERRIDE=${OPT_REPO_OVERRIDE}"
			shift
			;;
		--help) # display syntax
			show_opts
			exit 0
			;;
		-*|--*=) # unsupported flags
			echo "ERROR: Unsupported option $1" >&2
			show_opts
			exit 1
			;;
		*) # all other parameters pass through
			PARAMS="$PARAMS $1"
			shift
			;;
	esac
done

# reset positional arguments
eval set -- "$PARAMS"

# Get version information and download URL for pre-built JAR files
if [ ${OPT_REPO_OVERRIDE} ]; then
	echo "INFO: Overriding Repo Location=${XILINX_DEVSCRIPT_VART_REPO}"
	# Update environment variable and derivatives used to download models
	XILINX_DEVSCRIPT_VART_REPO=${OPT_REPO_OVERRIDE}
	XILINX_DEVSCRIPT_VART_MODEL_ZOO=$XILINX_DEVSCRIPT_VART_REPO/models/AI-Model-Zoo
	XILINX_DEVSCRIPT_VART_MODEL_LIST=$XILINX_DEVSCRIPT_VART_MODEL_ZOO/model-list
	XILINX_DEVSCRIPT_VART_MODEL_DOWNLOADS=$XILINX_DEVSCRIPT_VART_MODEL_ZOO/model-downloads

	echo "################################################"
	echo "INFO: New Locations"
	echo "INFO: Repo            : ${XILINX_DEVSCRIPT_VART_REPO}"
	echo "INFO: Model Zoo       : ${XILINX_DEVSCRIPT_VART_MODEL_ZOO}"
	echo "INFO: Model List      : ${XILINX_DEVSCRIPT_VART_MODEL_LIST}"
	echo "INFO: Model Downloads : ${XILINX_DEVSCRIPT_VART_MODEL_DOWNLOADS}"
	echo "################################################"
fi

# Check for Model List in Vitis-AI Repository
if [ ! -d $XILINX_DEVSCRIPT_VART_MODEL_ZOO ]; then
	echo "## $_self: Vitis-AI Model Zoo (Repository) NOT FOUND [$XILINX_DEVSCRIPT_VART_MODEL_ZOO]"
	exit 1
fi

# Get a list of models using readarray
cd $XILINX_DEVSCRIPT_VART_MODEL_LIST
readarray -d '' TMPMODELS < <(find . -mindepth 1 -type d -print0)
TMPMODELNUM=${#TMPMODELS[@]}

echo "## $_self: $TMPMODELNUM models found"

# Create downloads directory
if [ ! -d $XILINX_DEVSCRIPT_VART_MODEL_DOWNLOADS ]; then
	echo "## $_self: Creating model download folder $XILINX_DEVSCRIPT_VART_MODEL_DOWNLOADS"
	mkdir -p $XILINX_DEVSCRIPT_VART_MODEL_DOWNLOADS
fi

# Loop through models and automate use of the downloader script
model_index=0
download_count=0
replace_count=0
skip_count=0
fail_count=0
total_urls=0
for model in "${TMPMODELS[@]}"; do
	echo "########################################################################################################"
	echo "## Model: [$((model_index+1)) of $TMPMODELNUM] - $(basename $model)"	
	echo "########################################################################################################"

	#cd $(basename $model)
	cd $XILINX_DEVSCRIPT_VART_MODEL_LIST/$(basename $model)

	# Get a list of downloads for this model
	TMPURLS=($(cat model.yaml | grep "download link" | cut -d: -f2-))
	# Get a list of checksums for this model
	TMPCHKSUM=($(cat model.yaml | grep "checksum" | cut -d: -f2-))

	# Change to downloads folder
	cd $XILINX_DEVSCRIPT_VART_MODEL_DOWNLOADS

	# Loop through downloads
	url_index=0
	for url in "${TMPURLS[@]}"; do
		model_checksum="${TMPCHKSUM[$url_index]}"
		model_file=$(echo "$url" | cut -d= -f2-)
		# local loop control
		do_download=0
		do_replace=0
		total_urls=$((total_urls+1))

		# Check for existing download and verify checksum
		if [ -f "$model_file" ] && [ -f "$model_file.md5" ]; then
			
			# See if checksum in MD5 file matches model.yaml entry
			tmp_checksum=($(cat "$model_file.md5" | cut -d ' ' -f1))
			if [ ! "$tmp_checksum" == "$model_checksum" ]; then
				if [ $FLAG_DEBUG -ne 0 ]; then echo "## Checksum does not match model.yaml - will re-download"; fi
				do_download=1
				do_replace=1
			else
				# Verify checksum
				if ! md5sum --status -c "$model_file.md5"; then
					if [ $FLAG_DEBUG -ne 0 ]; then echo "## Checksum Failed - will re-download"; fi
					do_download=1
					do_replace=1
				else
					if [ $FLAG_DEBUG -ne 0 ]; then echo "## Skipping [$((url_index+1)) of ${#TMPURLS[@]}] - (Checksum Match) [$model_file]/[$model_checksum] = [$tmp_checksum]"; fi
					skip_count=$((skip_count+1))				
				fi
			fi
		else
			do_download=1
		fi

		if [ $do_download == 1 ]; then
			if [ $do_replace == 1 ]; then
				echo "## REPLACING"
				if [ $FLAG_DEBUG -ne 0 ]; then echo "## Remove   [$((url_index+1)) of ${#TMPURLS[@]}] - (File/ChecksumFile) [$model_file]/[$model_file.md5]"; fi
				rm -f $model_file "$model_file.md5"
				replace_count=$((replace_count+1))
			fi
			echo "## Download [$((url_index+1)) of ${#TMPURLS[@]}] - (File/Checksum) [$model_file]/[$model_checksum]"

			# Create MD5 File
			echo "$model_checksum $model_file" > "$model_file.md5"

			if [ $FLAG_DEBUG -ne 0 ]; then
				wget -nv $url -O $model_file
			else
				wget -q $url -O $model_file
			fi

			# Verify Checksum
			if ! md5sum --status -c "$model_file.md5"; then
				echo "## Checksum Failed   - [$((url_index+1)) of ${#TMPURLS[@]}]"
				echo "## File              - [$model_file]"
				echo "## Checksum File     - [$model_file.md5]"
				echo "## Checksum          - [$model_checksum]"
				echo "## Computed Checksum - [$(md5sum $model_file | cut -d ' ' -f1)]"
				fail_count=$((fail_count+1))
			else
				download_count=$((download_count+1))
			fi
		fi

		# Increment url index
		url_index=$((url_index+1))
	done

	if [ $FLAG_DEBUG -ne 0 ]; then echo "########################################################################################################"; fi
	if [ $FLAG_DEBUG -ne 0 ]; then echo "## Downloaded:       $download_count of $total_urls"; fi
	if [ $FLAG_DEBUG -ne 0 ]; then echo "## Skipped:          $skip_count of $total_urls"; fi
	if [ $FLAG_DEBUG -ne 0 ]; then echo "## Replaced:         $replace_count"; fi
	if [ $FLAG_DEBUG -ne 0 ]; then echo "## Downloads Failed: $fail_count"; fi

	# Increment model index
	model_index=$((model_index+1))
done

echo "########################################################################################################"
echo "## Summary: ($_self)"
echo "########################################################################################################"
echo "## Total Models: ${#TMPMODELS[@]}"
echo "## Downloaded:   $download_count of $total_urls"
echo "## Skipped:      $skip_count of $total_urls"
echo "########################################################################################################"
echo "## Replaced:     $replace_count"
echo "########################################################################################################"
echo "## Downloads Failed: $fail_count"
echo "########################################################################################################"

replace_count=0
download_count=0
skip_count=0
fail_count=0