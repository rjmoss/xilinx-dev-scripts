#!/bin/bash

# Reference: KV260 Smartcam application deployment instructions
# - https://xilinx.github.io/kria-apps-docs/main/build/html/docs/smartcamera/docs/app_deployment.html
# https://pixabay.com/videos/download/video-8358_large.mp4?attachment
#
BASE_URL="https://pixabay.com/videos/download"

declare -A downloads_url
declare -A downloads_filename

downloads_filename[0]="video-8358_large.mp4"
downloads_filename[1]="video-8698_large.mp4"

# This downloads vitis ai library demo images and videos locally

_self="${0##*/}"

# define syntax
function show_syntax {
	echo "## Syntax:"
	echo "## -------"
	echo "## $_self --<option>"
	echo "##"
	echo "## Valid Options:"
	echo "## --------------"
	echo "## --debug"
	echo "##"
	echo "## 	Enable debug output"
	echo "##"
}

# Init command ling argument flags
FLAG_DEBUG=0 # Enable extra debug messages

# Process Command line arguments
PARAMS=""

while (("$#")); do
	case "$1" in
		--debug) # Enable debug output
			FLAG_DEBUG=1
			echo "Set: FLAG_DEBUG=$FLAG_DEBUG"
			shift
			;;
		-*|--*=) # unsupported flags
			echo "ERROR: Unsupported option $1" >&2
			show_syntax
			exit 1
			;;
		*) # all other parameters pass through
			PARAMS="$PARAMS $1"
			shift
			;;
	esac
done

# reset positional arguments
eval set -- "$PARAMS"

BASE_DOWNLOAD_URL="https://www.xilinx.com/bin/public/openDownload?filename="

download_file="vitis_ai_library_r1.4.0_video.tar.gz"

if [ $FLAG_DEBUG -ne 0 ]; then 
	wget -nv -x --show-progress \
		$BASE_DOWNLOAD_URL"${download_file}" \
		-O $download_file
else
	wget -q -x \
		$BASE_DOWNLOAD_URL"${download_file}" \
		-O $download_file
fi
