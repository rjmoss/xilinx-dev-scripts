#!/bin/bash

# Reference: Vitis AI Examples
# - https://www.xilinx.com/html_docs/vitis_ai/1_4/running_examples.html#ariaid-title3
# - https://www.xilinx.com/bin/public/openDownload?filename=vitis_ai_runtime_r1.4.0_image_video.tar.gz

# This downloads vitis ai library demo images and videos locally

_self="${0##*/}"

# define syntax
function show_syntax {
	echo "## Syntax:"
	echo "## -------"
	echo "## $_self --<option>"
	echo "##"
	echo "## Valid Options:"
	echo "## --------------"
	echo "## --debug"
	echo "##"
	echo "## 	Enable debug output"
	echo "##"
}

# Init command ling argument flags
FLAG_DEBUG=0 # Enable extra debug messages

# Process Command line arguments
PARAMS=""

while (("$#")); do
	case "$1" in
		--debug) # Enable debug output
			FLAG_DEBUG=1
			echo "Set: FLAG_DEBUG=$FLAG_DEBUG"
			shift
			;;
		-*|--*=) # unsupported flags
			echo "ERROR: Unsupported option $1" >&2
			show_syntax
			exit 1
			;;
		*) # all other parameters pass through
			PARAMS="$PARAMS $1"
			shift
			;;
	esac
done

# reset positional arguments
eval set -- "$PARAMS"

BASE_DOWNLOAD_URL="https://www.xilinx.com/bin/public/openDownload?filename="

download_file="vitis_ai_runtime_r1.4.0_image_video.tar.gz"

if [ $FLAG_DEBUG -ne 0 ]; then 
	wget -nv -x --show-progress \
		$BASE_DOWNLOAD_URL"${download_file}" \
		-O $download_file
else
	wget -q -x \
		$BASE_DOWNLOAD_URL"${download_file}" \
		-O $download_file
fi
