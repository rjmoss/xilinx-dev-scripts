#!/bin/bash
# Declare Demo Media Alias (functions)

# Define command line function to prep the environment for Vitis AI Runtime SDK (Petalinux)
xilinx_media_showfiles () {
	# Show xilinx demo media files
	echo "Listing Demo Videos:"
	echo "--------------------"
	ls -al $XILINX_DEVSCRIPT_DEMO_VIDEOS
	echo "Listing Demo Images:"
	echo "--------------------"
	ls -al $XILINX_DEVSCRIPT_DEMO_IMAGES
}

xilinx_help_demo_media_alias () {
	# Print
	printf "%-${XILINX_HELP_WIDTH_1}s | %-${XILINX_HELP_WIDTH_1}s\n" \
		"* Xilinx Demo Media Aliases" "Description"
	printf "%.${XILINX_HELP_WIDTH_1}s | %.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR2 $XILINX_SEPARATOR2

	# Aliases List
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		" - xilinx_media_showfiles" "Show xilinx demo media file list"
}

# List Aliases
xilinx_help_demo_media_alias

printf "\n"

#echo "Demo Media Aliases:"
#echo "-------------------"
#declare -f | grep xilinx_media*
#echo "------------------------"
