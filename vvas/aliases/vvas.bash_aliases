#!/bin/bash
# Declare Vitis Video Analytics SDK Alias (functions)

# Check for presence of vitis ai runtime tools in the path
if [[ ! -f $(which vitis) ]]; then
	echo "WARNING: Vitis not located in system PATH (No Vitis AI Runtime aliases will be available)."
fi

xilinx_help_vvas_alias () {
	# Print
	printf "%-${XILINX_HELP_WIDTH_1}s | %-${XILINX_HELP_WIDTH_1}s\n" \
		"* Xilinx Vitis Video Analytics SDK" "Description"
	printf "%.${XILINX_HELP_WIDTH_1}s | %.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR2 $XILINX_SEPARATOR2

	# Aliases List
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		" - <none>" "<none>"
}

# List Aliases
xilinx_help_vvas_alias

printf "\n"

#echo "VVAS Aliases:"
#echo "------------------------"
#declare -f | grep xilinx_*
#echo "------------------------"

