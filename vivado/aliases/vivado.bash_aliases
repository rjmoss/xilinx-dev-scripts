#!/bin/bash
# Declare Vivado Alias (functions)

# Check for presence of vivado tools in the path
if [[ ! -f $(which vivado) ]]; then
	echo "WARNING: Vivado not located in system PATH (No Vivado aliases will be available)."
fi

# Define command line function to prep the environment for launching the vitis gui
xilinx_prep_vivado () {
	# Get the Vitis Version from the environment
	TMP_VERSION=$(basename $XILINX_VIVADO)
	pushd $XILINX_DEVSCRIPT_VIVADO_WORK/$TMP_VERSION
}

xilinx_help_vivado_alias () {
	# Print
	printf "%-${XILINX_HELP_WIDTH_1}s | %-${XILINX_HELP_WIDTH_1}s\n" \
		"* Xilinx Vivado Aliases" "Description"
	printf "%.${XILINX_HELP_WIDTH_1}s | %.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR2 $XILINX_SEPARATOR2

	# Aliases List
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		" - xilinx_prep_vivado" "Prep shell for Vivado development"
}

# List Aliases
xilinx_help_vivado_alias
printf "\n"

#echo "Vivado Aliases:"
#echo "---------------"
#declare -f | grep xilinx_prep_vivado*
#echo "---------------"
