#!/bin/bash
# Script to setup development aliases for common workflow tasks
echo "##### Executing ${BASH_SOURCE[0]} #####"

# Set this folder as the include path
XILINX_DEVSCRIPT_REPOBASE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export XILINX_DEVSCRIPT_REPOBASE

echo "Adding Xilinx Devtool Repository Aliases..."
###################################
# Add Tool Aliases
###################################
# - Demo MEdia
. $XILINX_DEVSCRIPT_REPOBASE/media/aliases/demo-media.bash_aliases

# - Petalinux
. $XILINX_DEVSCRIPT_REPOBASE/petalinux/aliases/petalinux.bash_aliases

# - Vivado
. $XILINX_DEVSCRIPT_REPOBASE/vivado/aliases/vivado.bash_aliases

# - Vitis
. $XILINX_DEVSCRIPT_REPOBASE/vitis/aliases/vitis.bash_aliases

# - Vitis AI
. $XILINX_DEVSCRIPT_REPOBASE/vitis_ai/aliases/vitis_ai.bash_aliases

# - Vitis AI Runtime
. $XILINX_DEVSCRIPT_REPOBASE/vart/aliases/vart.bash_aliases

# - Xilinx Target Aliases
# - SSH
. $XILINX_DEVSCRIPT_REPOBASE/target/aliases/target-ssh.bash_aliases

# - Vitis Video Analytics SDK Aliases
#. $XILINX_DEVSCRIPT_REPOBASE/vvas/aliases/vvas.bash_aliases

# - Vitis Libraries
. $XILINX_DEVSCRIPT_REPOBASE/vitis_libraries/aliases/vitis_libraries.bash_aliases

# - Tools and other utilities built from source
. $XILINX_DEVSCRIPT_REPOBASE/tools/aliases/tools.bash_aliases

# - Vitis AI Docker Container tools and scripts
. $XILINX_DEVSCRIPT_REPOBASE/vitis_ai_docker/aliases/vitis_ai_docker.bash_aliases
. $XILINX_DEVSCRIPT_REPOBASE/vitis_ai_docker/aliases/vitis_ai_docker_helper.bash_aliases

########################################################################################

xilinx_debug_separator1 () {
	# Header
	printf "%.${XILINX_HELP_WIDTH_1}s$XILINX_SEPARATOR1_3%.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR1 $XILINX_SEPARATOR1
}

xilinx_debug_separator2 () {
	# Header
	printf "%.${XILINX_HELP_WIDTH_1}s$XILINX_SEPARATOR2_3%.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR2 $XILINX_SEPARATOR2
}

xilinx_debug_separator3 () {
	# Header
	printf "%.${XILINX_HELP_WIDTH_1}s$XILINX_SEPARATOR3_3%.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR3 $XILINX_SEPARATOR3
}

###################################
# Display Help for all aliases
###################################
xilinx_help () {
	# Header
	xilinx_debug_separator1
	printf " Xilinx Alias Help Commands"
	declare -F | grep xilinx_help_ | awk '{ print $3 }'

	xilinx_debug_separator1
	printf " Xilinx Sript Help Commands"
	find $PATH -maxdepth 1 -type f -name "xilinx_help*"

	# footer
	xilinx_debug_separator1
}

xilinx_help_alias () {
	# Header
	xilinx_debug_separator1
	
	xilinx_help_demo_media_alias
	xilinx_debug_separator3

	xilinx_help_petalinux_alias
	xilinx_debug_separator3

	xilinx_help_target_alias
	xilinx_debug_separator3

	xilinx_help_tools_alias
	xilinx_debug_separator3

	xilinx_help_vitis_ai_alias
	xilinx_debug_separator3
	
	xilinx_help_vart_alias
	xilinx_debug_separator3

	xilinx_help_vitis_alias
	xilinx_debug_separator3

	xilinx_help_vitis_libraries_alias
	xilinx_debug_separator3

	xilinx_help_vivado_alias
	xilinx_debug_separator3

	xilinx_help_vitis_ai_docker_alias
	
	# footer
	xilinx_debug_separator1
}