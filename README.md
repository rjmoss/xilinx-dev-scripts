# xilinx-dev-scripts

Personal collection of helpful bash scripts to working with Xilinx tools and automating modification and configuration of FPGA project files.


## Quickstart
This is a quickstart reference for using these scripts on my Linux Host Machine but also inside of docker containers as part of my development workflow (see [xilinx-docker](https://gitlab.com/rjmoss/xilinx-docker) for more information on docker containers).

### Clone this repository on a Linux Host
```bash
mkdir -p ~/repositories/gitlab
pushd ~/repositories/gitlab
git clone https://gitlab.com/rjmoss/xilinx-dev-scripts.git
popd
```

### Add these scripts to your Linux System Path
- Source the top level path setup script in your ```.bashrc``` file to include these scripts in your shell PATH
- Add the following to your ```~/.bashrc``` file:
```bash
...
# Setup paths and environment variables for xilinx tool scripts
source ~/repositories/gitlab/xilinx-dev-scripts/set_env.sh
source ~/repositories/gitlab/xilinx-dev-scripts/set_paths.sh
source ~/repositories/gitlab/xilinx-dev-scripts/set_aliases.sh
```

## Host Requirements

### Executing SSH Commands on Targets in scripts (with passwords)
- Install the ```sshpass``` tool
```bash
sudo apt-get install sshpass
```

## Bash Script Information
- Special Variables
- Note: Markdown Table Editing using https://github.com/vkocubinsky/SublimeTableEditor

| Special Variables | Description |
| ----------------- | ----------- |
| $0 | The name of the bash script |
| $1, $2, ... $n | The base script arguments |
| $$ | The process id of the current shell |
| $# | The total number of arguments passed to the script |
| $@ | The value of all the arguments passed to the script |
| $? | The exit status of the last executed command |
| $! | The process id of the last executed command |



