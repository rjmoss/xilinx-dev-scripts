#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Run a Xilinx Vitis AI Docker container as an image
# - The image should run in daemon mode and mount relevant folders/files from the host for development
# - The image should allow the host to use Vitis AI tools from a locally launched interactive shell

# This script uses the following shell variables as default values
# ----------------------------------------------
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_CPULIMIT_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT
_docker_image="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT}"
_docker_image_version="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT}"
_docker_container_name="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT}"
_docker_cpu_limit=${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_CPULIMIT_DEFAULT}
_docker_run_path=$(realpath ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT})

# Show info
echo "# ${_self}"
echo "# - Image     : ${_docker_image}"
echo "# - Version   : ${_docker_image_version}"
echo "# - Container : ${_docker_container_name}"
echo "# - CPU Limit : ${_cpu_limit}"

# Check for existing container
if [ "$(xilinx_vai_docker_container_exists)" == "1" ]; then
	# Container found, no need to create another one, check if running
	if [ "$(xilinx_vai_docker_container_isrunning)" == "1" ]; then
		# A running container was found - DO NOT FORCE REMOVE a running container
		echo "# ERROR: A Running Container [${_docker_container_name}] was found!"
		echo "# ERROR: Refusing to FORCEFULLY remove running container."
		echo "# INFO: Please stop the container before removal."
		exit 1
	else
		echo "# INFO: Removing Container [${_docker_container_name}]."
		set -x # Turn off command echo to shell

		docker rm ${_docker_container_name}

		{ set +x; } 2>/dev/null # Turn off command echo
	fi
else
	# Container not found, create one
	echo "# INFO: Unable to remove container [${_docker_container_name}]."
	echo "# INFO: Container does not exist."
fi
