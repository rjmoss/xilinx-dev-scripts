#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Run a Xilinx Vitis AI Docker container as an image
# - The image should run in daemon mode and mount relevant folders/files from the host for development
# - The image should allow the host to use Vitis AI tools from a locally launched interactive shell

# This script uses the following shell variables as default values
# ----------------------------------------------
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_CPULIMIT_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT
_docker_image="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT}"
_docker_image_version="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT}"
_docker_container_name="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT}"
_docker_cpu_limit=${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_CPULIMIT_DEFAULT}
_docker_run_path=$(realpath ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT})

# Show info
echo "# ${_self}"
echo "# - Image     : ${_docker_image}"
echo "# - Version   : ${_docker_image_version}"
echo "# - Container : ${_docker_container_name}"
echo "# - CPU Limit : ${_cpu_limit}"

# Check for existing container
if [ "$(xilinx_vai_docker_container_exists)" == "0" ]; then
	# Container not found, exit
	echo "# INFO: Container [${_docker_container_name}] not found!"
	exit 1
fi

echo "# INFO: Restarting container [${_docker_container_name}]."

# Enable sharing of x session
xhost +

set -x # Turn on command echo to shell

# Run the docker container
docker restart ${_docker_container_name}

{ set +x; } 2>/dev/null # Turn off command echo
