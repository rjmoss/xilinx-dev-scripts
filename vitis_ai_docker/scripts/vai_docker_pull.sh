#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Run a Xilinx Vitis AI Docker container as an image
# - The image should run in daemon mode and mount relevant folders/files from the host for development
# - The image should allow the host to use Vitis AI tools from a locally launched interactive shell

# This script uses the following shell variables as default values
# ----------------------------------------------
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_CPULIMIT_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT
_docker_image="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT}"
_docker_image_version="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT}"
_docker_container_name="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT}"
_docker_cpu_limit=${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_CPULIMIT_DEFAULT}
_docker_run_path=$(realpath ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT})

# Show info
echo "# ${_self}"
echo "# - Image     : ${_docker_image}"
echo "# - Version   : ${_docker_image_version}"
echo "# - Container : ${_docker_container_name}"
echo "# - CPU Limit : ${_cpu_limit}"

# Check for valid paths
if [ ! -d ${_docker_run_path} ]; then
	echo "# ERROR: Working directory not found ! [${_docker_run_path}]."
	echo "# Exiting."
	exit 1
fi

# Check for existing image
if [ "$(xilinx_vai_docker_image_exists)" == "0" ]; then
	
	echo "# ERROR: Image not found locally, checking [${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_REPO_NAME}] docker repo"
	# Query docker image in repo
	if [ "$(xilinx_vai_docker_image_inrepo)" == "0" ]; then
		# Image not found in repo
		echo "# ERROR: Image ${_docker_image} not found in repo!"
		exit 1
	fi

	# Query image tags in repo
	if [ "$(xilinx_vai_docker_image_tag_inrepo)" == "0" ]; then
		# No version match found
		echo "# ERROR: Image [${_docker_image}] version [${_docker_image_version}] not found!"
		exit 1
	fi

	echo "# INFO: Pulling [${_docker_container_name}] from repo"

	set -x # Turn off command echo to shell

	# To pull quietly, add "-q" to the pull command
	docker pull ${_docker_image}:${_docker_image_version}

	{ set +x; } 2>/dev/null # Turn off command echo

	# Verify that image exists now in local cache
	# Check for existing container
	if [ "$(xilinx_vai_docker_image_exists)" == "0" ]; then
		# Image not found, pull must have failed!
		echo "# ERROR: [${_docker_image}:${_docker_image_version}] not found locally - PULL failed!"
		exit 1
	fi
else
	echo "# INFO: Image [${_docker_image}] exists in local cache."
fi

set -x # Turn on command echo to shell

docker image ls ${_docker_image}:${_docker_image_version}

{ set +x; } 2>/dev/null # Turn off command echo
