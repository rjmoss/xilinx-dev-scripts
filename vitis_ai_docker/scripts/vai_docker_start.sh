#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Run a Xilinx Vitis AI Docker container as an image
# - The image should run in daemon mode and mount relevant folders/files from the host for development
# - The image should allow the host to use Vitis AI tools from a locally launched interactive shell

# This script uses the following shell variables as default values
# ----------------------------------------------
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_CPULIMIT_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT
_docker_image="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT}"
_docker_image_version="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT}"
_docker_container_name="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT}"
_docker_cpu_limit=${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_CPULIMIT_DEFAULT}
_docker_run_path=$(realpath ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT})

# Show info
echo "# ${_self}"
echo "# - Image     : ${_docker_image}"
echo "# - Version   : ${_docker_image_version}"
echo "# - Container : ${_docker_container_name}"
echo "# - CPU Limit : ${_cpu_limit}"

# Check for valid paths
if [ ! -d ${_docker_run_path} ]; then
	echo "# ERROR: Working directory not found ! [${_docker_run_path}]."
	echo "# Exiting."
	exit 1
fi

# Check for existing image
if [ "$(xilinx_vai_docker_image_exists)" == "0" ]; then
	echo "# ERROR: Image not found locally, cannot create container."
	exit 1
fi

# Check for existing container
if [ "$(xilinx_vai_docker_container_exists)" == "1" ]; then
	# Container found, no need to create another one, check if running
	if [ "$(xilinx_vai_docker_container_isrunning)" == "1" ]; then
		echo "# INFO: Container [${_docker_container_name}] already running!"
	else
		echo "# INFO: Container [${_docker_container_name}] exists but is not running!"
		echo "# INFO: Restarting container [${_docker_container_name}]."
		set -x # Turn off command echo to shell
		docker restart ${_docker_container_name}
		{ set +x; } 2>/dev/null # Turn off command echo
	fi
else
	# Container not found, create one
	echo "# Creating docker container [${_docker_container_name}]"

	# Set parameters
	_docker_default_command="bash"
	_docker_run_mode="-itd"
	_docker_uid=$(id -u)
	_docker_guid=$(id -g)
	_docker_user=${USER}
	_docker_remove_on_exit="" # or "--rm"
	
	if [ "$(xilinx_vai_docker_image_isgpu)" == "1" ];then
		_docker_use_gpus="--gpus all"
	else
		_docker_use_gpus=""
	fi

	# Enable sharing of x session
	xhost +

	set -x # Turn on command echo to shell

	# Run the docker container
	docker run \
		--name ${_docker_container_name} \
		--cpus ${_docker_cpu_limit} \
		${_docker_use_gpus} \
		-e USER=${_docker_user} \
		-e UID=${_docker_uid} \
		-e GID=${_docker_guid} \
		-e VERSION=${_docker_image_version} \
		-e DISPLAY=${DISPLAY} \
		\
		-v ${_docker_run_path}:/vitis_ai_home \
	    -v ${_docker_run_path}:/workspace \
		-v /tmp/.X11-unix:/tmp/.X11-unix \
		-v /home/${_docker_user}/.Xauthority:/home/xilinx/.Xauthority \
		-v ~/repositories:/home/xilinx/repositories \
		-v /xilinx:/xilinx \
		-v /srv/tftpboot:/srv/tftpboot \
		-v /srv/software:/srv/software \
	    -v /dev/shm:/dev/shm \
	    -v /opt/xilinx/dsa:/opt/xilinx/dsa \
	    -v /opt/xilinx/overlaybins:/opt/xilinx/overlaybins \
	    -w /workspace \
	    --network=host \
	    --hostname $(hostname) \
	    ${_docker_remove_on_exit} \
	    ${_docker_run_mode} \
	    ${_docker_image}:${_docker_image_version} \
	    ${_docker_default_command}

	# Create an XTerm Config File
	docker exec -it \
		${_docker_container_name} \
		bash -c '\
			echo "XTerm*Background: black" >> ~/XTerm ;\
			echo "XTerm*Foreground: green" >> ~/XTerm ;\
			echo "XTerm*savelines: 1000000" >> ~/XTerm ;\
			echo "XTerm*faceName: Ubuntu mono" >> ~/XTerm ;\
			echo "XTerm*faceSize: 10" >> ~/XTerm ;\
			echo "XTerm*rightScrollBar: true" >> ~/XTerm ;\
			echo "XTerm*ScrollBar: true" >> ~/XTerm ;\
			echo "XTerm*selectToClipboard: true" >> ~/XTerm ;\
			'

	# Show XTerm Config File
	docker exec -it \
		${_docker_container_name} \
		bash -c '\
			cat ~/XTerm ;\
			'

	# Update the package cache and install utilities
	# Note: imagemagick is used with Python Image Library (PIL) show() function
	docker exec -it \
		${_docker_container_name} \
		bash -c '\
			sudo apt-get update ;\
			sudo apt-get install -y xterm \
				imagemagick \
				graphviz \
				p7zip;\
			'

	{ set +x; } 2>/dev/null # Turn off command echo
fi