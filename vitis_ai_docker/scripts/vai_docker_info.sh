#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Show Information about Vitis AI Docker Images
docker images "*/vitis-ai*"

# Show Information about Vitis AI Containers
docker container ls

