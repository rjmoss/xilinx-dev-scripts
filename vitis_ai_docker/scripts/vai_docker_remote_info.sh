#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Show Information about Vitis AI Docker Images in Remote Hub
docker search "xilinx/vitis-ai"

# Show remote tags
_remote_images=$(docker search "xilinx/vitis-ai" --format '{{.Name}}')
_remote_image_array=($_remote_images)

# Loop through image names
_remote_url=${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_REGISTRY_BASE_URL}
for _image in "${_remote_image_array[@]}"; do
	_image_tags=$(wget -qO - "${_remote_url}/${_image}/tags" | jq -r '.results[].name' | paste -sd,)
	echo "# Image : ${_image}"
	echo "          [ Tags = ${_image_tags} ]"
done
