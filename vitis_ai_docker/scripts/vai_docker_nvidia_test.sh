#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Verify that NVIDIA GPU is accessible inside of docker

# Variables
WORKDIR=~/tmp_docker_nvidia
CUDA_VER=10.2
IMAGE_NAME=nvidia-test
CONTAINER_NAME=nvidia_test_${CUDA_VER}

# Create a temporary folder
mkdir -p ${WORKDIR}
pushd ${WORKDIR} >/dev/null 2>&1

# Check to see if image exists
_exists=$(docker image inspect ${IMAGE_NAME}:${CUDA_VER} >/dev/null 2>&1)
if [ "$?" == "1" ]; then
   # Note: If inspect returns 1 the image does not exist
   # Build the image

	# Create a temporary Dockerfile
	echo "FROM nvidia/cuda:${CUDA_VER}-base" > Dockerfile
	echo "CMD nvidia-smi" >> Dockerfile

	# Build a temporary docker image
	docker build . -t ${IMAGE_NAME}:${CUDA_VER} -f Dockerfile
fi

_exists=$(docker image inspect ${IMAGE_NAME}:${CUDA_VER} >/dev/null 2>&1)
if [ "$?" == "1" ]; then
	# Note: If inspect returns 1 the image does not exist
	echo "NVIDIA TEST FAILED - No Docker Image Found!"
	exit 1
fi

# Run the docker container
docker run \
	--gpus all \
	--name ${CONTAINER_NAME} \
	${IMAGE_NAME}:${CUDA_VER}

# Stop and shutdown the docker container
docker stop ${CONTAINER_NAME} >/dev/null 2>&1

# Remove docker container
docker rm ${CONTAINER_NAME} >/dev/null 2>&1

popd >/dev/null 2>&1