#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Run a Xilinx Vitis AI Docker container as an image
# - The image should run in daemon mode and mount relevant folders/files from the host for development
# - The image should allow the host to use Vitis AI tools from a locally launched interactive shell

# This script uses the following shell variables as default values
# ----------------------------------------------
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_CPULIMIT_DEFAULT
# XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT
_docker_image="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT}"
_docker_image_version="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT}"
_docker_container_name="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT}"
_docker_cpu_limit=${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_CPULIMIT_DEFAULT}
_docker_run_path=${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT}

# Show info
echo "# ${_self}"
echo "# - Container : ${_docker_container_name}"

# Check for existing container
if [ "$(xilinx_vai_docker_container_exists)" == "1" ]; then
	if [ "$(xilinx_vai_docker_container_isrunning)" == "0" ]; then
		# Container not running, exit
		echo "# INFO: A Running Container [${_docker_container_name}] not found!"
		exit 0
	else
		echo "# INFO: Attaching to Docker Container [${_docker_container_name}]"
		echo "# INFO: To disconnect gracefull, use CTRL-P + CTRL-Q"
		#set -x # Turn on command echo to shell

		# Create an XTerm Session
		docker attach \
			${_docker_container_name}

		#{ set +x; } 2>/dev/null # Turn off command echo
		echo "# INFO: Detatching from Docker Container [${_docker_container_name}]"
	fi
else
	echo "# INFO: Container [${_docker_container_name}] not found!"
	exit 0
fi
