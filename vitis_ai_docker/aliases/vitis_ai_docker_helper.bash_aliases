#!/bin/bash
#Note: Defaults are set but can be overridden by setting them in the local environment
#Note: This file must be included after the following:

# Export all environment variables
set -a

# DOCKER IMAGE MANAGEMENT
#############################
# xilinx_vai_docker_image_exists
# - "0" if image does not exist
# - "1" if image exists
xilinx_vai_docker_image_exists() {
    _exists=$(docker image inspect ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT}:${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT} >/dev/null 2>&1)
    if [ "$?" == "1" ]; then
        # Note: If inspect returns 1 the image does not exist
        echo "0"
    else
        echo "1"
    fi
}

# xilinx_vai_docker_image_inrepo
# - "0" if image does not exist in the docker repo (remote)
# - "1" if image exists in the docker repo (remote)
xilinx_vai_docker_image_inrepo() {
    _in_repo=$(docker search ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT} --format '{{.Name}}' 2> /dev/null)
    if [[ "${_in_repo}" == "" ]]; then
        # Note: If inspect returns 1 the image does not exist
        echo "0"
    else
        echo "1"
    fi
}

# xilinx_vai_docker_image_tag_inrepo
# - "0" if image tag does not exist in the docker repo (remote)
# - "1" if image tag exists in the docker repo (remote)
xilinx_vai_docker_image_tag_inrepo() {
    _tags_in_repo=$(wget -qO - "${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_REGISTRY_BASE_URL}${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT}/tags" | jq -r '.results[].name')
    _found=""
    for current_tag in ${_tags_in_repo}
    do
        if [[ "$current_tag" == "${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT}" ]]; then
            # Found image version match
            _found=${current_tag}
            break
        fi
    done
    if [[ "${_found}" ]]; then
        echo "1"
    else
        echo "0"
    fi
}

# xilinx_vai_docker_image_isgpu
# - "0" if image is NOT gpu accelerated
# - "1" if image is a gpu accelerated image
xilinx_vai_docker_image_isgpu() {
    _dockertype=$(docker image inspect ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT}:${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT} \
         --format='{{.Config.Env}}' | \
         tr ' ' '\n' | \
         grep DOCKER_TYPE | \
         awk -F= '{print($2)}' | \
         tr -d '()')

    if [ "${_dockertype}" == "GPU" ]; then
        echo "1"
    else
        echo "0"
    fi
}

# xilinx_vai_docker_image_iscpu
# - "0" if image is NOT a cpu based image
# - "1" if image is a cpu based image
xilinx_vai_docker_image_iscpu() {
    _dockertype=$(docker image inspect ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT}:${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT} \
         --format='{{.Config.Env}}' | \
         tr ' ' '\n' | \
         grep DOCKER_TYPE | \
         awk -F= '{print($2)}' | \
         tr -d '()')
    
    if [ "${_dockertype}" == "CPU" ]; then
        echo "1"
    else
        echo "0"
    fi
}

# DOCKER CONTAINER MANAGEMENT
#############################
# xilinx_vai_docker_container_isrunning:
# - "0" if container is not running
# - "1" if container is running
xilinx_vai_docker_container_isrunning() {
    _is_running=$(docker inspect ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT} --format='{{.State.Running}}')
    if [ "${_is_running}" == "false" ]; then
        echo "0"
    else
        echo "1"
    fi
}

# xilinx_vai_docker_container_exists:
# - "0" if container does not exist
# - "1" if container exists
xilinx_vai_docker_container_exists() {
    _exists=$(docker ps -aq -f name=${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT})
    if [ "${_exists}" ]; then
        echo "1"
    else
        echo "0"
    fi
}

