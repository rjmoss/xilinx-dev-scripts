#!/bin/bash
# Declare Vitis AI Docker Alias (functions)

# Check for presence of vitis ai runtime tools in the path
if [[ ! -f $(which vitis) ]]; then
	echo "WARNING: Vitis not located in system PATH (No Vitis Tools are available)."
fi

# Define command line function to checkout copy of Vitis AI Yocto Layer
# Note: This should be run in the root petalinux project folder
# xilinx_prep_vitis_ai_meta_layer () {
# 	# Repo: https://github.com/Xilinx/meta-vitis-ai
# 	# First check current folder for Petalinux Project
# 	TMP_DIR=$(pwd)	
# 	TMP_PLNX_VERSION=($(cat "$TMP_DIR/.petalinux/metadata" | cut -d '=' -f2))
# 	if [[ -z $TMP_PLNX_VERSION ]]; then
# 		echo "Petalinux Project Not Found (.petalinux/metadata is missing or corrupt)"
# 		echo "Please execute this script in the root folder of a valid Petalinux Project"
# 		exit 1
# 	else
# 		echo "Petalinux Project v$TMP_PLNX_VERSION detected"
# 	fi

# 	TMP_LAYER=$XILINX_DEVSCRIPT_VITIS_AI_META_LAYER
# 	TMP_REPO=$XILINX_DEVSCRIPT_VITIS_AI_META_LAYER_REPO
# 	TMP_TAG=$XILINX_DEVSCRIPT_VITIS_AI_META_LAYER_REPO_TAGPREFIX$TMP_PLNX_VERSION

# 	# Checkout the vitis ai layer from github
# 	echo "Checkout The Vitis AI Layer: $TMP_REPO"
# 	git clone  ./project-spec/$TMP_LAYER
# 	pushd ./project-spec/$TMP_LAYER
# 	git checkout -b working_$TMP_PLNX_VERSION 
# 	popd

# 	# Add layer to petalinux project

# 	# Add packagegroup to user layer

# 	# Enable petalinux package group in user layer
# 	# use existing helper script
# 	update_plnx_packagegroup petalinux-vitisai
# }


xilinx_vai_docker_info () {
	echo "Vitis AI Docker Defaults (this shell)"
	echo "-------------------------------------"
	echo "- Image Type          : ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT}"
	echo "- Image Name          : ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT}"
	echo "- Image Version       : ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT}"
	echo "- Container Name      : ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT}"
	echo "- Container CPU Limit : ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_CPULIMIT_DEFAULT}"
	echo "- Local Host Runpath  : ${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT}"
	echo "- Vitis AI Repo Path  : ${XILINX_DEVSCRIPT_VART_REPO}"
}

# Temporarily override the version of Vitis AI docker container in the current shell
# Uses $1 as the version number
xilinx_vai_docker_set_version () {
	if [ -z "$1" ]; then
		echo "ERROR: Vitis AI Version Number required"
	else
		case "$1" in
			"1.4" | "1.4*") # VERSION_VAI=1.4.1.978, VERSION_VAI=1.4.916
				echo "OVERRIDE: Using Vitis AI Version ($1) defaults instead of (${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT})"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT="1.4.1.978"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT="xilinx_vitis_ai_${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT}_${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT}"
				export XILINX_DEVSCRIPT_VART_REPO="~/repositories/github/Xilinx/Vitis-AI/1.4.1/"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT=${XILINX_DEVSCRIPT_VART_REPO}
				;;
			"2.0")
				echo "OVERRIDE: Using Vitis AI Version ($1) defaults instead of (${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT})"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT="2.0"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT="xilinx_vitis_ai_${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT}_${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT}"
				export XILINX_DEVSCRIPT_VART_REPO="~/repositories/github/Xilinx/Vitis-AI/2.0/"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT=${XILINX_DEVSCRIPT_VART_REPO}
				;;
			"2.5")
				echo "OVERRIDE: Using Vitis AI Version ($1) defaults instead of (${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT})"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT="2.5"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT="xilinx_vitis_ai_${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT}_${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT}"
				export XILINX_DEVSCRIPT_VART_REPO="~/repositories/github/Xilinx/Vitis-AI/2.5/"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT=${XILINX_DEVSCRIPT_VART_REPO}
				;;
			*)
				echo "ERROR: Version ($1) defaults not valid or not supported"
				;;
		esac
	fi
}

xilinx_vai_docker_get_version () {
	echo "Using Vitis AI Version (${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT})"
}

xilinx_vai_docker_set_image_type () {
	if [ -z "$1" ]; then
		echo "ERROR: Vitis AI Image Type Name required (gpu, cpu)!"
	else
		case "$1" in
			"cpu" | "CPU" )
				echo "OVERRIDE: Using Vitis AI Image Type (cpu) instead of (${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT})"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT="cpu"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_REPO_NAME}vitis-ai-${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT}"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT="xilinx_vitis_ai_${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT}_${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT}"
				;;
			"gpu" | "GPU" )
				echo "OVERRIDE: Using Vitis AI Image Type (gpu) instead of (${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT})"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT="gpu"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_REPO_NAME}vitis-ai-${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT}"
				export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT="xilinx_vitis_ai_${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT}_${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT}"
				;;
			*)
				echo "ERROR: Image Type ($1) not supported.  Must be (cpu, gpu)."
				;;
		esac
	fi
}

xilinx_vai_docker_get_image_type () {
	echo "Using Vitis AI Image Type (${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT})"
}

xilinx_help_vitis_ai_docker_alias () {
	# Print
	printf "%-${XILINX_HELP_WIDTH_1}s | %-${XILINX_HELP_WIDTH_1}s\n" \
		"* Xilinx Vitis AI Docker Aliases" "Description"
	printf "%.${XILINX_HELP_WIDTH_1}s | %.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR2 $XILINX_SEPARATOR2

	# Aliases List
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		"- xilinx_vai_docker_info " "Show current configuration/shell variables for Vitis AI Docker Helper Scripts"
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		"- xilinx_vai_docker_get_version " "Show current Vitis AI Version for this shell session"
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		"- xilinx_vai_docker_set_version <version_number>" "Override default Vitis AI Version for this shell session"
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		"- xilinx_vai_docker_get_image_type " "Show current Vitis AI Image Type for this shell session"
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		"- xilinx_vai_docker_set_image_type <cpu,gpu>" "Override default Vitis AI Image Type for this shell session"
	#printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
	#	" - xilinx_prep_vitis_ai_meta_layer" "Checkout the meta-vitis-ai layer and add to current petalinux project."
}

# List Aliases
xilinx_help_vitis_ai_docker_alias

printf "\n"
