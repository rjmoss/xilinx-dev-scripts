#!/bin/bash
# Script to execute Xilinx tool scripts during a shell session
# Add sourcing of this file in your ~/.bashrc file
#echo "##### Executing ${BASH_SOURCE[0]} #####"

# Single shell session override of tool revision
# - Override tool revision in the next shell instance
# - Accomplished through conditional pre-processing of $XILINX_DEVSCRIPT_DEFAULT_BASHRC_OVERRIDE file

if [ -f $XILINX_DEVSCRIPT_DEFAULT_BASHRC_OVERRIDE ]; then
	echo " - Checking file ${XILINX_DEVSCRIPT_DEFAULT_BASHRC_OVERRIDE}"
	echo " ##"
	cat $XILINX_DEVSCRIPT_DEFAULT_BASHRC_OVERRIDE
	echo " ##"

	# read in override value from file
	. $XILINX_DEVSCRIPT_DEFAULT_BASHRC_OVERRIDE

	echo " - Override tool version for this session - (${XILINX_DEVSCRIPT_TOOL_VERSION})"

	# remove the override file so it only applies to the current shell session
	rm -f $XILINX_DEVSCRIPT_DEFAULT_BASHRC_OVERRIDE
else
	XILINX_DEVSCRIPT_TOOL_VERSION=$XILINX_DEVSCRIPT_DEFAULT_TOOL_VERSION
	echo " - Using defaults - (${XILINX_DEVSCRIPT_TOOL_VERSION})"
fi

echo " - Setting up Xilinx Tools Version (v${XILINX_DEVSCRIPT_TOOL_VERSION})"
# Execute appropriate tool version setup scripts
case $XILINX_DEVSCRIPT_TOOL_VERSION in
	2021.2)
		########################################################################################
		# v2021.2 tools
		########################################################################################
		source /opt/tools/Xilinx/Petalinux/2021.2/settings.sh
		source /opt/tools/Xilinx/Vitis/2021.2/settings64.sh
		;;
	2021.1)
		########################################################################################
		# v2021.1 tools
		########################################################################################
		source /opt/tools/Xilinx/Petalinux/2021.1/settings.sh
		source /opt/tools/Xilinx/Vitis/2021.1/settings64.sh
		;;
	*)
		echo " - No tool setup scripts executed"
		;;
esac	
