#!/bin/bash
# Declare Tool Alias Functions

# Define command line function to prep the environment for using Vitis Vision Libraries
# v2021.2 - OpenCV 4.4 + Cmake > 3.5.1 (v3.22)
xilinx_prep_tools_cmake_3-22 () {
	XILINX_DEVSCRIPT_TOOLS_CMAKE_PATH=/xilinx/local/tools/cmake/3.22/bin
	echo " CMAKE: ${XILINX_DEVSCRIPT_TOOLS_CMAKE_PATH}"

	# Set the PATH variable to use selected CMAKE vs. Vitis built-in
	export PATH=$XILINX_DEVSCRIPT_TOOLS_CMAKE_PATH:$PATH
}

xilinx_prep_tools_opencv_4-4-0 () {
	XILINX_DEVSCRIPT_TOOLS_OPENCV_PATH=/xilinx/local/tools/opencv/4.4.0/bin
	echo " CMAKE: ${XILINX_DEVSCRIPT_TOOLS_OPENCV_PATH}"

	# Set the PATH variable to use selected CMAKE vs. Vitis built-in
	export PATH=$XILINX_DEVSCRIPT_TOOLS_OPENCV_PATH:$PATH
}

xilinx_tools_balena_etcher () {
	echo " Executing Balena Etcher Tool."
	/xilinx/local/tools/balenaEtcher/balenaEtcher-1.5.122-x64.AppImage
}

xilinx_prep_tools_bashrc_override_v2021.1 () {
	echo " Setting Bash Override to 2021.1 in ${XILINX_DEVSCRIPT_DEFAULT_BASHRC_OVERRIDE}"
	echo "XILINX_DEVSCRIPT_TOOL_VERSION=2021.1" > $XILINX_DEVSCRIPT_DEFAULT_BASHRC_OVERRIDE
}

xilinx_help_tools_alias () {
	# Print
	printf "%-${XILINX_HELP_WIDTH_1}s | %-${XILINX_HELP_WIDTH_1}s\n" \
		"* Xilinx Tool Aliases" "Description"
	printf "%.${XILINX_HELP_WIDTH_1}s | %.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR2 $XILINX_SEPARATOR2

	# Aliases List
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		"- xilinx_prep_tools_cmake_3-22" "add cmake 3.22 to the system path"
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		"- xilinx_prep_tools_opencv_4-4-0" "add opencv 4.4.0 to the system path"
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		"- xilinx_prep_tools_bashrc_override_v2021.1" "temporarily override bashrc to source 2021.1 version setup scripts"
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		"- xilinx_tools_balena_etcher" "launch the balena etcher app image"
}

# List aliases
xilinx_help_tools_alias

printf "\n"

#echo "Tools and Libs from source:"
#echo "------------------------"
#declare -f | grep xilinx_prep_tools*
#declare -f | grep xilinx_tools*
#echo "------------------------"