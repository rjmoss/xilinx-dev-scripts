#!/bin/bash
# Script to setup common environment variables for use with scripts in this repository
echo "##### Executing ${BASH_SOURCE[0]} #####"

########################################################################################
# Define help and debug output spacing parameters
########################################################################################
XILINX_HELP_WIDTH_1=50
XILINX_HELP_WIDTH_2=60
XILINX_SEPARATOR_1_3="###"
XILINX_SEPARATOR_1_10="##########"
XILINX_SEPARATOR_1_20=$XILINX_SEPARATOR_1_10$XILINX_SEPARATOR_1_10
XILINX_SEPARATOR_1_50=$XILINX_SEPARATOR_1_10$XILINX_SEPARATOR_1_20_$XILINX_SEPARATOR_1_20
XILINX_SEPARATOR_1_100=$XILINX_SEPARATOR_1_50$XILINX_SEPARATOR_1_50
XILINX_SEPARATOR1=$XILINX_SEPARATOR_1_100

XILINX_SEPARATOR_2_3="---"
XILINX_SEPARATOR_2_10="----------"
XILINX_SEPARATOR_2_20=$XILINX_SEPARATOR_2_10$XILINX_SEPARATOR_2_10
XILINX_SEPARATOR_2_50=$XILINX_SEPARATOR_2_10$XILINX_SEPARATOR_2_20_$XILINX_SEPARATOR_2_20
XILINX_SEPARATOR_2_100=$XILINX_SEPARATOR_2_50$XILINX_SEPARATOR_2_50
XILINX_SEPARATOR2=$XILINX_SEPARATOR_2_100

XILINX_SEPARATOR_3_3="~~~"
XILINX_SEPARATOR_3_10="~~~~~~~~~~"
XILINX_SEPARATOR_3_20=$XILINX_SEPARATOR_3_10$XILINX_SEPARATOR_3_10
XILINX_SEPARATOR_3_50=$XILINX_SEPARATOR_3_10$XILINX_SEPARATOR_3_20_$XILINX_SEPARATOR_3_20
XILINX_SEPARATOR_3_100=$XILINX_SEPARATOR_3_50$XILINX_SEPARATOR_3_50
XILINX_SEPARATOR3=$XILINX_SEPARATOR_3_100

########################################################################################
# Define default configurations for global scripts
########################################################################################
# Default tool version information used in bashrc shell setup for new tool sessions
# See ./tools/scripts/set_toolscripts_bashrc.sh for example
XILINX_DEVSCRIPT_DEFAULT_TOOL_VERSION=2022.1
XILINX_DEVSCRIPT_DEFAULT_BASHRC_OVERRIDE=~/.bashrc_xilinx

export XILINX_DEVSCRIPT_DEFAULT_TOOL_VERSION
export XILINX_DEVSCRIPT_DEFAULT_BASHRC_OVERRIDE

########################################################################################
# Define base working system paths
########################################################################################
# - Demo Media (Images, Video)
XILINX_DEVSCRIPT_DEMO_MEDIA=/xilinx/local/media
XILINX_DEVSCRIPT_DEMO_VIDEOS=$XILINX_DEVSCRIPT_DEMO_MEDIA/videos
XILINX_DEVSCRIPT_DEMO_IMAGES=$XILINX_DEVSCRIPT_DEMO_MEDIA/images

export XILINX_DEVSCRIPT_DEMO_MEDIA
export XILINX_DEVSCRIPT_DEMO_VIDEOS
export XILINX_DEVSCRIPT_DEMO_IMAGES

# - Petalinux Related
XILINX_DEVSCRIPT_PLNX_WORK=/xilinx/local/shared/petalinux
XILINX_DEVSCRIPT_PLNX_DOWNLOADS=/xilinx/local/downloads
XILINX_DEVSCRIPT_PLNX_MIRRORS=/xilinx/local/sstate-mirrors

# - Export for use in other scripts and in the shell environment
export XILINX_DEVSCRIPT_PLNX_WORK
export XILINX_DEVSCRIPT_PLNX_DOWNLOADS
export XILINX_DEVSCRIPT_PLNX_MIRRORS

########################################################################################
# - Xilinx Repository Information
XILINX_DEVSCRIPT_GITHUB_REPO=https://github.com/Xilinx

# - Export for use in other scripts and in the shell environment
export XILINX_DEVSCRIPT_GITHUB_REPO

########################################################################################
# - Vivado Related
XILINX_DEVSCRIPT_VIVADO_WORK=/xilinx/local/shared/vivado

# - Export for use in other scripts and in the shell environment
export XILINX_DEVSCRIPT_VIVADO_WORK

########################################################################################
# - Vitis Related
XILINX_DEVSCRIPT_VITIS_WORK=/xilinx/local/shared/vitis
XILINX_DEVSCRIPT_VITIS_PLATFORMS=/xilinx/local/platforms

# - Export for use in other scripts and in the shell environment
export XILINX_DEVSCRIPT_VITIS_WORK
export XILINX_DEVSCRIPT_VITIS_PLATFORMS

########################################################################################
# - Vitis AI Related
XILINX_DEVSCRIPT_VITIS_AI_META_LAYER=meta-vitis-ai
XILINX_DEVSCRIPT_VITIS_AI_META_LAYER_REPO=$XILINX_DEVSCRIPT_GITHUB_REPO/$XILINX_DEVSCRIPT_VITIS_AI_META_LAYER
XILINX_DEVSCRIPT_VITIS_AI_META_LAYER_REPO_TAGPREFIX=xlnx-rel-v

# - Export for use in other scripts and in the shell environment
export XILINX_DEVSCRIPT_VITIS_AI_META_LAYER
export XILINX_DEVSCRIPT_VITIS_AI_META_LAYER_REPO
export XILINX_DEVSCRIPT_VITIS_AI_META_LAYER_REPO_TAGPREFIX

########################################################################################
# - Vitis AI Runtime Related
#XILINX_DEVSCRIPT_VART_REPO=~/repositories/github/Xilinx/Vitis-AI
#XILINX_DEVSCRIPT_VART_REPO=~/repositories/github/Xilinx/Vitis-AI/1.4.1/
#XILINX_DEVSCRIPT_VART_REPO=~/repositories/github/Xilinx/Vitis-AI/2.0/
XILINX_DEVSCRIPT_VART_REPO=~/repositories/github/Xilinx/Vitis-AI/2.5/
XILINX_DEVSCRIPT_VART_SDKS=/xilinx/local/shared/vitis-ai-sdk
#Note: Vitis AI 2.5 restructured the repository, so the model zoo has moved
#XILINX_DEVSCRIPT_VART_MODEL_ZOO=$XILINX_DEVSCRIPT_VART_REPO/models/AI-Model-Zoo
XILINX_DEVSCRIPT_VART_MODEL_ZOO=$XILINX_DEVSCRIPT_VART_REPO/model_zoo
XILINX_DEVSCRIPT_VART_MODEL_LIST=$XILINX_DEVSCRIPT_VART_MODEL_ZOO/model-list
XILINX_DEVSCRIPT_VART_MODEL_DOWNLOADS=$XILINX_DEVSCRIPT_VART_MODEL_ZOO/model-downloads

# - Export for use in other scripts and in the shell environment
export XILINX_DEVSCRIPT_VART_REPO
export XILINX_DEVSCRIPT_VART_SDKS
export XILINX_DEVSCRIPT_VART_MODEL_ZOO
export XILINX_DEVSCRIPT_VART_MODEL_LIST
export XILINX_DEVSCRIPT_VART_MODEL_DOWNLOADS

########################################################################################
# - Vitis AI Docker Container Related
XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT="gpu" # "gpu" or "cpu"
XILINX_DEVSCRIPT_VITIS_AI_DOCKER_REPO_NAME="xilinx/"
XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_REPO_NAME}vitis-ai-${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT}"
XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT="2.5" # 1.4.1.978, 2.0, 2.5
XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT="xilinx_vitis_ai_${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT}_${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT}"
XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_CPULIMIT_DEFAULT=8
XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT=${XILINX_DEVSCRIPT_VART_REPO}

# - Export for use in other scripts and in the shell environment
expprt XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_TYPE_DEFAULT
export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT
export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_VERSION_DEFAULT
export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_NAME_DEFAULT
export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_CONTAINER_CPULIMIT_DEFAULT
export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_RUNPATH_DEFAULT

# - Vitis AI Docker Registry Related
XILINX_DEVSCRIPT_VITIS_AI_DOCKER_REGISTRY_BASE_URL="https://registry.hub.docker.com/v2/repositories/"
XILINX_DEVSCRIPT_VITIS_AI_DOCKER_REGISTRY_TAGS_URL="${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_REGISTRY_BASE_URL}${XILINX_DEVSCRIPT_VITIS_AI_DOCKER_IMAGE_DEFAULT}/tags"

# - Export for use in other scripts and in the shell environment
export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_REGISTRY_TAGS_URL
export XILINX_DEVSCRIPT_VITIS_AI_DOCKER_REGISTRY_BASE_URL

########################################################################################
# - Vitis Video Analytics SDK Related
XILINX_DEVSCRIPT_VVAS_REPO=~/repositories/github/Xilinx/VVAS
#XILINX_DEVSCRIPT_VART_SDKS=/xilinx/local/shared/vitis-ai-sdk
#XILINX_DEVSCRIPT_VART_MODEL_ZOO=$XILINX_DEVSCRIPT_VART_REPO/models/AI-Model-Zoo
#XILINX_DEVSCRIPT_VART_MODEL_LIST=$XILINX_DEVSCRIPT_VART_MODEL_ZOO/model-list
#XILINX_DEVSCRIPT_VART_MODEL_DOWNLOADS=$XILINX_DEVSCRIPT_VART_MODEL_ZOO/model-downloads

# - Export for use in other scripts and in the shell environment
export XILINX_DEVSCRIPT_VVAS_REPO
#export XILINX_DEVSCRIPT_VART_SDKS
#export XILINX_DEVSCRIPT_VART_MODEL_ZOO
#export XILINX_DEVSCRIPT_VART_MODEL_LIST
#export XILINX_DEVSCRIPT_VART_MODEL_DOWNLOADS

########################################################################################
# - Vitis Libraries Related
# - Vision Libraries v2021.2 requires OpenCV 4.4.0
# - OpenCV 4.4.0 requires CMAKE 3.5.1 or greater (latest stable 3.22)
XILINX_DEVSCRIPT_VITIS_LIBRARIES_OPENCV_PATH=/xilinx/local/tools/opencv/4.4.0/bin
XILINX_DEVSCRIPT_VITIS_LIBRARIES_CMAKE_PATH=/xilinx/local/tools/cmake/3.22/bin

# - Export for use in other scripts and in the shell environment
export XILINX_DEVSCRIPT_VITIS_LIBRARIES_OPENCV_PATH
export XILINX_DEVSCRIPT_VITIS_LIBRARIES_CMAKE_PATH

# See AR33097
# - https://support.xilinx.com/s/article/Vitis-2021-1-Libraries-Compiling-and-Installing-OpenCV?language=en_US
XILINX_DEVSCRIPT_VITIS_LIBRARIES_OPENCV_INCLUDE_PATH=/xilinx/local/tools/opencv/4.4.0/include/opencv4
XILINX_DEVSCRIPT_VITIS_LIBRARIES_OPENCV_LIB_PATH=/xilinx/local/tools/opencv/4.4.0/lib

# Export for other scripts
export XILINX_DEVSCRIPT_VITIS_LIBRARIES_OPENCV_INCLUDE_PATH
export XILINX_DEVSCRIPT_VITIS_LIBRARIES_OPENCV_LIB_PATH

########################################################################################
# - Open Source Tool Related
#   Any libraries or supporting software built from source repositories
# - OpenCV 4.4.0 requires CMAKE 3.5.1 or greater (latest stable 3.22)
XILINX_DEVSCRIPT_TOOLS_OPENCV_PATH=/xilinx/local/tools/opencv/4.4.0/bin
XILINX_DEVSCRIPT_TOOLS_CMAKE_PATH=/xilinx/local/tools/cmake/3.22/bin

# - Export for use in other scripts and in the shell environment
export XILINX_DEVSCRIPT_TOOLS_OPENCV_PATH
export XILINX_DEVSCRIPT_TOOLS_CMAKE_PATH

########################################################################################
# - Xilinx Target Related

# - Target SSH Related

# - Target VART Related
