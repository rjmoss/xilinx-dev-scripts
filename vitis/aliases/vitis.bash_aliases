#!/bin/bash
# Declare Vitis Alias (functions)

# Check for presence of vitis tools in the path
if [[ ! -f $(which vitis) ]]; then
	echo "WARNING: Vitis not located in system PATH (No Vitis aliases will be available)."
fi

# Define command line function to prep the environment for launching the vitis gui
xilinx_prep_vitis () {
	# Get the Vitis Version from the environment
	# Export the appropriate PLATFORM REPO folder
	TMP_VER=$(basename $XILINX_VITIS)
	export PLATFORM_REPO_PATH=$XILINX_DEVSCRIPT_VITIS_PLATFORMS/${TMP_VER}

	# Change to a default working directory
	#pushd $XILINX_DEVSCRIPT_VITIS_WORK/${TMP_VER}
}

xilinx_help_vitis_alias () {
	# Print
	printf "%-${XILINX_HELP_WIDTH_1}s | %-${XILINX_HELP_WIDTH_1}s\n" \
		"* Xilinx Vitis Aliases" "Description"
	printf "%.${XILINX_HELP_WIDTH_1}s | %.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR2 $XILINX_SEPARATOR2

	# Aliases List
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		" - xilinx_prep_vitis" "Prep shell for Vitis development"
}

# List Aliases
xilinx_help_vitis_alias

printf "\n"
#echo "Vitis Aliases:"
#echo "--------------"
#declare -f | grep xilinx_prep_vitis*
#echo "--------------"
