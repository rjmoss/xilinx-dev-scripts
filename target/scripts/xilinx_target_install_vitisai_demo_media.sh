#!/bin/bash

# Reference: Vitis AI Tutorial Board Dependencies Reference
# - https://github.com/Xilinx/Vitis-AI-Tutorials/blob/master/Introduction/03-Basic/Module_2/Board_Dependency/get_image_video_zcu104.sh
# wget -O vitis_ai_library_r1.3.0_images.tar.gz https://www.xilinx.com/bin/public/openDownload?filename=vitis_ai_library_r1.3.0_images.tar.gz
# wget -O vitis_ai_library_r1.3.0_video.tar.gz https://www.xilinx.com/bin/public/openDownload?filename=vitis_ai_library_r1.3.0_video.tar.gz

_self="${0##*/}"

# define syntax
function show_syntax {
	echo "## Syntax:"
	echo "## -------"
	echo "## $_self <target_ip_address>"
	echo "## "
}

# Check For Valid IP Address
TMPIP=`echo "$1" | awk '/^([0-9]{1,3}[.]){3}([0-9]{1,3})$/{print $1}'`
if [ -z $TMPIP ]; then
	echo "## $_self: [$TMPIP] appears to be an invalid IP Address."
	show_syntax
	exit 1
fi

# Check that IP Address is reachable
ping -c 1 $TMPIP
if [ ! $? ]; then
	echo "## $_self: [$TMPIP] is unreachable"
	exit 1
fi

# Download Vitis AI Demo Images and Videos to target filesystem
# XILINX_DEVSCRIPT_VART_REPO is base path for the Vitis-AI Repository
# TMPPATH is the destination path on the target
TMPPATH="~/"

echo "## $_self: copying VART installation packages to $TMPIP:$TMPPATH"

sshpass -p 'root' scp -r \
	-o StrictHostKeyChecking=no \
	$XILINX_DEVSCRIPT_VART_REPO/setup/mpsoc \
	root@$TMPIP:~/

echo "## $_self: listing remote file(s) on $TMPIP:$TMPPATH"

sshpass -p 'root' ssh -X \
	-o StrictHostKeyChecking=no \
	root@$TMPIP \
	ls -al $TMPPATH

# Execute the Target VART Setup Script
TMPPATH="~/mpsoc/VART"
TMPSCRIPT="target_vart_setup.sh"

echo "## $_self: executing vart setup script $TMPPATH/$TMPSCRIPT on $TMPIP"

sshpass -p 'root' ssh -X \
	-o StrictHostKeyChecking=no \
	root@$TMPIP \
	"cd $TMPPATH && pwd && ./$TMPSCRIPT"
