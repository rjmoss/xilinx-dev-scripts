#!/bin/bash
# Declare aliases and runtime functions for working with target platforms over SSH connections

# Define a generic ssh function for connecting to and executing commands on Xilinx target hardware
# The `-X` option denotes X11 Forwarding from the target platform
# The `-o StrictHostKeyChecking=no` option bypasses the host key check that requires user interaction
#
# Usage: xilinx_ssh <target_ip_address> <commands to execute>
# where: $1 = <target_ip_address>
# where: ${@:2} = <commands to execute>

xilinx_ssh () {
	# Check for a valid IP address
	FN=${FUNCNAME[0]}
	TMP=`echo "$1" | awk '/^([0-9]{1,3}[.]){3}([0-9]{1,3})$/{print $1}'`
	if [ -z $TMP ]; then
		echo "$FN: [$1] appears to be an invalid IP Address."
		echo "Usage:"
		echo "------"
		echo "    $FN <target_ip_address> '<commands_to_execute>'"
	else
		echo "## $FN: executing ${@:2} on $1"
		sshpass -p 'root' ssh -X \
			-o StrictHostKeyChecking=no \
			root@$1 \
			"${@:2}"
	fi
}

# Define a generic scp function for copying a local file to Xilinx target hardware
# The `-o StrictHostKeyChecking=no` option bypasses the host key check that requires user interaction
#
# Usage: xilinx_scp <file_to_copy> <target_ip_address>:<target_folder>
#	     or
#        xilinx_scp <directory_to_copy> <target_ip_address>:<target_folder>
#
# where: $2 = <target_ip_address>:<target_folder>
# where: $1 = <file_to_copy> or <directory_to_copy>
xilinx_scp_to () {
	# Check for a valid IP address
	FN=${FUNCNAME[0]}
	TMPIP=`echo "$2" | cut -d: -f1`
	TMPPATH=`echo "$2" | awk -F: '{print $2}'`
	TMP=`echo "$TMPIP" | awk '/^([0-9]{1,3}[.]){3}([0-9]{1,3})$/{print $1}'`
	if [ -z $TMP ]; then
		echo "$FN: [$TMPIP] appears to be an invalid IP Address."
		echo "Usage:"
		echo "------"
		echo "    $FN <files_to_copy> <target_ip_address>:<target_folder>"
	else
		if [ -f $1 ]; then
			echo "## $FN: copying local file $1 to $TMPPATH on $TMPIP"
			
			sshpass -p 'root' scp \
				-o StrictHostKeyChecking=no \
				$1 \
				root@$2

		elif [ -d $1 ]; then
			echo "## $FN: copying local folder $1 to $TMPPATH on $TMPIP"
			
			sshpass -p 'root' scp \
				-o StrictHostKeyChecking=no \
				-r \
				$1 \
				root@$2
		fi

		echo "## $FN: listing remote file(s) $1 on $TMPIP:$TMPPATH"

		sshpass -p 'root' ssh -X \
			-o StrictHostKeyChecking=no \
			root@$TMPIP \
			ls -al $TMPPATH/$1
	fi
}

xilinx_help_target_alias () {
	# Print
	printf "%-${XILINX_HELP_WIDTH_1}s | %-${XILINX_HELP_WIDTH_1}s\n" \
		"* Xilinx Target Helper Aliases" "Description"
	printf "%.${XILINX_HELP_WIDTH_1}s | %.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR2 $XILINX_SEPARATOR2

	# Aliases List
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		" - xilinx_scp_to" "copy file or folder to target ip address as root"
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		" - xilinx_ssh" "connect to target ip address as root with X11 forwarding"
}

# List Aliases
xilinx_help_target_alias

printf "\n"

#echo "Xilinx Target SSH Aliases:"
#echo "--------------------------"
#declare -f | grep xilinx_ssh*
#declare -f | grep xilinx_scp*
#echo "--------------------------"
