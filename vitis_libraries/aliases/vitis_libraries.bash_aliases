#!/bin/bash
# Declare Vitis Libraries Alias (functions)

# Check for presence of vitis ai runtime tools in the path
if [[ ! -f $(which vitis) ]]; then
	echo "WARNING: Vitis not located in system PATH (No Vitis Tools are available)."
fi

# Define command line function to prep the environment for using Vitis Vision Libraries
# v2021.2 - OpenCV 4.4 + Cmake > 3.5.1 (v3.22)
xilinx_prep_vitis_libraries_vision () {
	# --- VISION Libraries ---
	# Setup the OpenCV path and CMAKE path for Vitis Vision Libraries
	# Override XILINX_DEVSCRIPT_VITIS_LIBRARIES_OPENCV_PATH
	# Override XILINX_DEVSCRIPT_VITIS_LIBRARIES_CMAKE_PATH
	unset LD_LIBRARY_PATH
	TMP_VER=$(basename $XILINX_VITIS)
	case $TMP_VER in
		2021.2)
			;&
		2021.1)
			XILINX_DEVSCRIPT_VITIS_LIBRARIES_CMAKE_PATH=/xilinx/local/tools/cmake/3.22/bin
			XILINX_DEVSRIPT_VITIS_LIBRARIES_OPENCV_PATH=/xilinx/local/tools/opencv/4.4.0/bin
			;;
		*)
			echo "Using Defaults"
			;;
	esac			

	echo "OPENCV: ${XILINX_DEVSCRIPT_VITIS_LIBRARIES_OPENCV_PATH}"
	echo " CMAKE: ${XILINX_DEVSCRIPT_VITIS_LIBRARIES_CMAKE_PATH}"

	# Set the PATH variable to use selected CMAKE and OpenCV vs. Vitis built-in
	export PATH=$XILINX_DEVSCRIPT_VITIS_LIBRARIES_CMAKE_PATH:$PATH
	export PATH=$XILINX_DEVSCRIPT_VITIS_LIBRARIES_OPENCV_PATH:$PATH

	# See AR33097
	# - https://support.xilinx.com/s/article/Vitis-2021-1-Libraries-Compiling-and-Installing-OpenCV?language=en_US
	export OPENCV_INCLUDE=$XILINX_DEVSCRIPT_VITIS_LIBRARIES_OPENCV_INCLUDE_PATH
	export OPENCV_LIB=$XILINX_DEVSCRIPT_VITIS_LIBRARIES_OPENCV_LIB_PATH
	export LD_LIBRARY_PATH=$XILINX_DEVSCRIPT_VITIS_LIBRARIES_OPENCV_LIB_PATH:$LD_LIBRARY_PATH

	echo "Set the following:"
	echo "- OPENCV_INCLUDE=$OPENCV_INCLUDE"
	echo "- OPENCV_LIB=$OPENCV_LIB"
	echo "- LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
}

xilinx_help_vitis_libraries_alias () {
	# Print
	printf "%-${XILINX_HELP_WIDTH_1}s | %-${XILINX_HELP_WIDTH_1}s\n" \
		"* Xilinx Vitis Libraries Aliases" "Description"
	printf "%.${XILINX_HELP_WIDTH_1}s | %.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR2 $XILINX_SEPARATOR2

	# Aliases List
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		" - xilinx_prep_vitis_libraries_vision" "Set CMAKE and OPENCV includes and libraries for Vitis Vision"
}

# List Aliases
xilinx_help_vitis_libraries_alias

printf "\n"

#echo "Vitis Libraries Aliases:"
#echo "------------------------"
#declare -f | grep xilinx_prep_vitis_libraries*
#echo "------------------------"

