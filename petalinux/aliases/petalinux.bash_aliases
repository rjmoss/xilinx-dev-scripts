#!/bin/bash
# Declare Petalinux Alias (functions)

# Check for presence of Petalinux tools in the path
if [[ ! -f $(which petalinux-build) ]]; then
	echo "WARNING: Petalinux not located in system PATH (No Petalinux aliases will be available)."
fi

# Define command line function to prep the environment for launching the vitis gui
xilinx_prep_petalinux () {
	# Get the Petalinux Version from the environment
	TMP_VERSION=$PETALINUX_VER
	pushd $XILINX_DEVSCRIPT_PLNX_WORK/$TMP_VERSION
}

# Define cross compile environment for manual compilation of kernel, u-boot, device-tree
# CROSS_COMPILE definitions:
# Zynq-7000 (soft float): arm-xilinx-linux-gnueabi-
# Zynq-7000 (hard float): arm-linux-gnueabihf-
# ZynqMPSoC / Versal    : aarch64-linux-gnu-
# Microblaze (LE)       : microblazeel-xilinx-linux-gnu-
# Microblaze (BE)       : microblaze-xilinx-linux-gnu-
# PowerPC               : powerpc-eabi-
xilinx_prep_petalinux_crosstools_zyqnmp () {
	# Export the Cross tools prefix
	export CROSS_COMPILE=aarch64-linux-gnu-
	echo "Cross Compiler Prefix: ${CROSS_COMPILE}"
	# Make Example for Linux Kernel
	echo "Example linux-xlnx:"
	echo " $ make ARCH=arm64 xilinx_zynqmpd_defconfig"
	echo " $ make menuconfig"
	echo " $ make ARCH=arm64"
}

xilinx_help_petalinux_alias () {
	# Print
	printf "%-${XILINX_HELP_WIDTH_1}s | %-${XILINX_HELP_WIDTH_1}s\n" \
		"* Xilinx Petalinux Aliases" "Description"
	printf "%.${XILINX_HELP_WIDTH_1}s | %.${XILINX_HELP_WIDTH_1}s\n" \
		$XILINX_SEPARATOR2 $XILINX_SEPARATOR2

	# Aliases List
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		" - xilinx_prep_petalinux" "Prep shell for Petalinux development"
	printf "%-${XILINX_HELP_WIDTH_1}s | %s\n" \
		" - xilinx_prep_petalinux_crosstools_zynqmp" "Prep shell for manual cross compilation"
}

# List Aliases
xilinx_help_petalinux_alias

printf "\n"
#echo "Petalinux Aliases:"
#echo "------------------"
#declare -f | grep xilinx_prep_petalinux*
#echo "------------------"
