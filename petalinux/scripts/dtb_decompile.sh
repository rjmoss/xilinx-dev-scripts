#!/bin/bash

_self="${0##*/}"

# Process Command line arguments
if [[ $# -ne 1 ]]; then
	echo "${@}"
	echo "This scripts requires one argument"
	echo "Syntax:"
	echo "$_self <device_tree_binary_blob>"
	echo "Examples:"
	echo "$_self system.dtb"
	echo "-----------------------------------------"
	exit 1
else
	if [ ! -f "${1}" ]; then
		echo "Device Tree Binary ${1} Not Found."
		exit 1
	fi
fi

# Test device tree compiler tool
DTC_BIN=$(which dtc)
DTB_FILE=${1}
DTS_FILE=$DTB_FILE.dts

if [ -f "${DTC_BIN}" ]; then
	dtc -I dtb -O dts -o $DTS_FILE $DTB_FILE
	echo "Decompiled $DTS_FILE from $DTB_FILE"
else
	echo "Device Tree Compiler 'dtc' Not Found."
fi
