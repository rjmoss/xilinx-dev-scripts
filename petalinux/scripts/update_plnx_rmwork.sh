#!/bin/bash

_self="${0##*/}"

CONFIG_FILE=project-spec/meta-user/conf/petalinuxbsp.conf
#CONFIG_FILE=test.txt

if [ ! -f "$CONFIG_FILE" ]; then
	echo "Petalinux Config $CONFIG_FILE Not Found."
	exit 1
fi

# define recipe work to exclude from cleanup stage
declare -A tmp_rmwork_recipes
TMP_RECIPE_COUNT=3
tmp_rmwork_recipes[0]="u-boot-xlnx"
tmp_rmwork_recipes[1]="device-tree"
tmp_rmwork_recipes[2]="linux-xlnx"


for (( i=0; i < $TMP_RECIPE_COUNT; i++ )); do
	KEY="RM_WORK_EXCLUDE.*=.*\".*${tmp_rmwork_recipes[$i]}.*\""
	REPLACE="RM_WORK_EXCLUDE += \" ${tmp_rmwork_recipes[$i]}\""
	# Test for existing entry
	if grep -Eq $KEY $CONFIG_FILE; then
		# Replace existing configuration line
		echo "Before: " && cat $CONFIG_FILE | grep $KEY
		sed -i "s@$KEY@$REPLACE@g" $CONFIG_FILE
		echo "After: " && cat $CONFIG_FILE | grep $KEY
	else
		# Add new configuration line
		echo $REPLACE >> $CONFIG_FILE
		echo "Added: " && cat $CONFIG_FILE | grep $KEY
	fi
done
