#!/bin/bash

_self="${0##*/}"

LAYER_CONFIG_FILE=project-spec/meta-user/conf/user-rootfsconfig
ROOTFS_CONFIG_FILE=project-spec/configs/rootfs_config

# Process Command line arguments
if [[ $# -ne 1 ]]; then
	echo "${@}"
	echo "This scripts requires one argument"
	echo "Syntax:"
	echo "$_self <package-group-name>"
	echo "Examples:"
	echo "$_self petalinux-vitisai"
	echo " - will enable CONFIG_packagegroup-petalinux-vitisai"
	echo "-----------------------------------------"
	exit 1
else
	if [ ! -f "$LAYER_CONFIG_FILE" ]; then
		echo "Petalinux Config $CONFIG_FILE Not Found."
		exit 1
	fi
	if [ ! -f "$ROOTFS_CONFIG_FILE" ]; then
		echo "Petalinux Config $CONFIG_FILE Not Found."
		exit 1
	fi
fi

KEY="CONFIG_packagegroup-${1}"
REPLACE="$KEY=y"

# Test for existing packagegroup configuration entry in the user layer
if grep -Fq $KEY $LAYER_CONFIG_FILE; then
	# Replace existing configuration line
	echo "Package group exists in user layer:" && cat $LAYER_CONFIG_FILE | grep $KEY
else
	# Add new string
	echo $KEY >> $LAYER_CONFIG_FILE
	echo "Package group added to user layer:" && cat $LAYER_CONFIG_FILE | grep $KEY
fi	

# Enable packagegroup in rootfs config in the project
if grep -Fq $KEY $ROOTFS_CONFIG_FILE; then
	# Replace existing configuration line
	echo "Replacing package group configuration"
	echo "Old:" && cat $ROOTFS_CONFIG_FILE | grep $KEY
	sed -i "s@^$KEY.*@$REPLACE@g" $ROOTFS_CONFIG_FILE
	echo "New:" && cat $ROOTFS_CONFIG_FILE | grep $KEY
else
	# Add new string
	echo $REPLACE >> $ROOTFS_CONFIG_FILE
	echo "Enabled package group:" && cat $ROOTFS_CONFIG_FILE | grep $KEY
fi	