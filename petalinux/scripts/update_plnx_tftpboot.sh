#!/bin/bash

_self="${0##*/}"

CONFIG_FILE=project-spec/configs/config

# Process Command line arguments
if [[ $# -ne 1 ]]; then
	echo "${@}"
	echo "This scripts requires one argument"
	echo "Syntax:"
	echo "$_self <tftpboot_path>"
	echo "Examples:"
	echo "$_self /srv/tftpboot/v2021.1"
	echo "-----------------------------------------"
	exit 1
else
	if [ ! -f "$CONFIG_FILE" ]; then
		echo "Petalinux Config $CONFIG_FILE Not Found."
		exit 1
	fi
	if [ ! -d "${1}" ]; then
		echo "Path: ${1} Not Found."
		exit 1
	fi
fi

KEY="CONFIG_SUBSYSTEM_TFTPBOOT_DIR"
REPLACE="$KEY=\"${1}\""

# Test for existing configuration entry
if grep -Fq $KEY $CONFIG_FILE; then
	# Replace existing configuration line
	echo "Before:" && cat $CONFIG_FILE | grep $KEY
	sed -i "s@^$KEY=.*@$REPLACE@g" $CONFIG_FILE
	echo "After:" && cat $CONFIG_FILE | grep $KEY
else
	# Add new string
	echo $REPLACE >> $CONFIG_FILE
	echo "Added:" && cat $CONFIG_FILE | grep $KEY
fi	
